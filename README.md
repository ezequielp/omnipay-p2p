# Omnipay: Place2Pay

**Place2Pay driver for the Omnipay PHP payment processing library**

## Installation

Omnipay is installed via [Composer](http://getcomposer.org/). To install, clone the repo

```
composer require omnipay/omnipay
```


## Basic Usage

The following gateways are provided by this package:

* Place2Pay

For general usage instructions, please see the main [Omnipay](https://github.com/thephpleague/omnipay)
repository.

## Support

If you are having general issues with Omnipay, we suggest posting on
[Stack Overflow](http://stackoverflow.com/). Be sure to add the
[omnipay tag](http://stackoverflow.com/questions/tagged/omnipay) so it can be easily found.

If you want to keep up to date with release anouncements, discuss ideas for the project,
or ask more detailed questions, there is also a [mailing list](https://groups.google.com/forum/#!forum/omnipay) which
you can subscribe to.
