<?php

namespace Omnipay\P2p;

use Omnipay\Tests\GatewayTestCase;

class GatewayTest extends GatewayTestCase
{
    /**
     * @var \Omnipay\P2p\Gateway
     */
    protected $gateway;

    public function setUp()
    {
        parent::setUp();

        $this->gateway = new Gateway($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testFetchIssuers()
    {
        $request = $this->gateway->fetchIssuers();

        $this->assertInstanceOf('Omnipay\P2p\Message\FetchIssuersRequest', $request);
    }

    public function testFetchPaymentMethods()
    {
        $request = $this->gateway->fetchPaymentMethods();

        $this->assertInstanceOf('Omnipay\P2p\Message\FetchPaymentMethodsRequest', $request);
    }

    public function testPurchase()
    {
        $request = $this->gateway->purchase(array('amount' => '10.00'));

        $this->assertInstanceOf('Omnipay\P2p\Message\PurchaseRequest', $request);
        $this->assertSame('10.00', $request->getAmount());
    }

    public function testPurchaseReturn()
    {
        $request = $this->gateway->completePurchase(array('amount' => '10.00'));

        $this->assertInstanceOf('Omnipay\P2p\Message\CompletePurchaseRequest', $request);
        $this->assertSame('10.00', $request->getAmount());
    }

    public function testRefund()
    {
        $request = $this->gateway->refund(
            array(
                'apiKey'               => 'key',
                'transactionReference' => 'tr_Qzin4iTWrU'
            )
        );

        $this->assertInstanceOf('Omnipay\P2p\Message\RefundRequest', $request);
        $data = $request->getData();
        $this->assertFalse(array_key_exists('amount', $data));
        $request = $this->gateway->refund(
            array(
                'apiKey'               => 'key',
                'transactionReference' => 'tr_Qzin4iTWrU',
                'amount'               => '10.00'
            )
        );

        $this->assertInstanceOf('Omnipay\P2p\Message\RefundRequest', $request);
        $data = $request->getData();
        $this->assertSame('10.00', $data['amount']);
    }

    public function testFetchTransaction()
    {
        $request = $this->gateway->fetchTransaction(
            array(
                'apiKey'               => 'key',
                'transactionReference' => 'tr_Qzin4iTWrU'
            )
        );

        $this->assertInstanceOf('Omnipay\P2p\Message\FetchTransactionRequest', $request);

        $data = $request->getData();
        $this->assertSame('tr_Qzin4iTWrU', $data['id']);
    }

    public function testCreateCustomer()
    {
        $request = $this->gateway->createCustomer(
            array(
                'description'  => 'Test name',
                'email'        => 'test@example.com',
                'metadata'     => 'Something something something dark side.',
                'locale'       => 'nl_NL'
            )
        );

        $this->assertInstanceOf('Omnipay\P2p\Message\CreateCustomerRequest', $request);
    }

    public function testUpdateCustomer()
    {
        $request = $this->gateway->updateCustomer(
            array(
                'customerReference' => 'cst_bSNBBJBzdG',
                'description'       => 'Test name2',
                'email'             => 'test@example.com',
                'metadata'          => 'Something something something dark side.',
                'locale'            => 'nl_NL'
            )
        );

        $this->assertInstanceOf('Omnipay\P2p\Message\UpdateCustomerRequest', $request);

        $data = $request->getData();

        $this->assertSame('cst_bSNBBJBzdG', $data['id']);
    }

    public function testFetchCustomer()
    {
        $request = $this->gateway->fetchCustomer(
            array(
                'apiKey'            => 'key',
                'customerReference' => 'cst_bSNBBJBzdG'
            )
        );

        $this->assertInstanceOf('Omnipay\P2p\Message\FetchCustomerRequest', $request);
    }
}
