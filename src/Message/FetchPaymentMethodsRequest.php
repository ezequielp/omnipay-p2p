<?php

namespace Omnipay\P2p\Message;

/**
 * P2p Fetch PaymentMethods Request
 *
 * @method \Omnipay\P2p\Message\FetchPaymentMethodsResponse send()
 */
class FetchPaymentMethodsRequest extends AbstractRequest
{
    /**
     * @return null
     */
    public function getData()
    {
        $this->validate('apiKey');
    }

    public function sendData($data)
    {
        $response = $this->sendRequest('GET', '/methods');

        return $this->response = new FetchPaymentMethodsResponse($this, $response);
    }
}
