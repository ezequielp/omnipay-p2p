<?php

namespace Omnipay\P2p;

use Omnipay\Common\AbstractGateway;

/**
 * P2P
 *
 * @link https://placetopay.github.io/web-checkout-api-docs/?shell#redirectrequest
 */
class Gateway extends AbstractGateway
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'P2p';
    }

    /**
     * @return array
     */
    public function getDefaultParameters()
    {
        return array(
            'apiKey' => ''
        );
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->getParameter('apiKey');
    }

    /**
     * @param  string $value
     * @return $this
     */
    public function setApiKey($value)
    {
        return $this->setParameter('apiKey', $value);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\FetchIssuersRequest
     */
    public function fetchIssuers(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\FetchIssuersRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\FetchPaymentMethodsRequest
     */
    public function fetchPaymentMethods(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\FetchPaymentMethodsRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\FetchTransactionRequest
     */
    public function fetchTransaction(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\FetchTransactionRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\PurchaseRequest
     */
    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\PurchaseRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\CompletePurchaseRequest
     */
    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\CompletePurchaseRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\RefundRequest
     */
    public function refund(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\RefundRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\CreateCustomerRequest
     */
    public function createCustomer(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\CreateCustomerRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\UpdateCustomerRequest
     */
    public function updateCustomer(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\UpdateCustomerRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\P2p\Message\FetchCustomerRequest
     */
    public function fetchCustomer(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\P2p\Message\FetchCustomerRequest', $parameters);
    }
}
